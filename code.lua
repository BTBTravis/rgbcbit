-- game1
-- by btbtravis

t = 0
debug = true
debugX = 1
debugY = 1
-- music(0)
activeBlaster = 1
activeColor = 'y'

bits = {}
add(bits, {})
add(bits, {})
add(bits, {})
add(bits, {})

track = {
  -- top || 1
 {x = 56, y = -5, dir = 'd'},
 {x = 56, y = 8, dir = 'l' },
 {x = 8, y = 8, dir = 'd' },
 {x = 8, y = 16, dir = 'r' },
 {x = 88, y = 16, dir = 'd' },
 {x = 88, y = 24, dir = 'l' },
 {x = 40, y = 24, dir = 'd' },
 {x = 40, y = 32, dir = 'r' },
 {x = 64, y = 32, dir = 'd' },
 {x = 64, y = 40, dir = 'l' },
 {x = 56, y = 40, dir = 'd' },
 {x = 56, y = 48, dir = 's' },
 -- left || 2
 {x = -5, y = 56, dir = 'r'},
 {x = 8, y = 56, dir = 'd'},
 {x = 8, y = 104, dir = 'r'},
 {x = 16, y = 104, dir = 'u'},
 {x = 16, y = 24, dir = 'r'},
 {x = 24, y = 24, dir = 'd'},
 {x = 24, y = 72, dir = 'r'},
 {x = 32, y = 72, dir = 'u'},
 {x = 32, y = 48, dir = 'r'},
 {x = 40, y = 48, dir = 'd'},
 {x = 40, y = 56, dir = 'r'},
 {x = 48, y = 56, dir = 's'},
 -- bottom || 3
 {x = 56, y = 129, dir = 'u'},
 {x = 56, y = 112, dir = 'r'},
 {x = 104, y = 112, dir = 'u'},
 {x = 104, y = 104, dir = 'l'},
 {x = 24, y = 104, dir = 'u'},
 {x = 24, y = 96, dir = 'r'},
 {x = 72, y = 96, dir = 'u'},
 {x = 72, y = 88, dir = 'l'},
 {x = 48, y = 88, dir = 'u'},
 {x = 48, y = 80, dir = 'r'},
 {x = 56, y = 80, dir = 'u'},
 {x = 56, y = 64, dir = 's'},
 -- right || 4
 {x = 129, y = 56, dir = 'l'},
 {x = 112, y = 56, dir = 'u'},
 {x = 112, y = 8, dir = 'l'},
 {x = 104, y = 8, dir = 'd'},
 {x = 104, y = 88, dir = 'l'},
 {x = 96, y = 88, dir = 'u'},
 {x = 96, y = 40, dir = 'l'},
 {x = 88, y = 40, dir = 'd'},
 {x = 88, y = 64, dir = 'l'},
 {x = 80, y = 64, dir = 'u'},
 {x = 80, y = 56, dir = 'l'},
 {x = 64, y = 56, dir = 's'},
}

function addBit(trackNum)
  local prevBit = bits[trackNum][#bits[trackNum]]
  local randomNum = flr(rnd(4))

  if (randomNum == 1) then
    randomColor = 'g'
  elseif (randomNum == 2) then
    randomColor = 'y'
  elseif (randomNum == 3) then
    randomColor = 'b'
  else
    randomColor = 'r'
  end


  local initX = 56 -- top
  local initY = -5
  if trackNum == 2 then -- left
    initX = -5
    initY = 56
  elseif trackNum == 3 then -- bottom
    initX = 56
    initY = 129
  elseif trackNum == 4 then -- right
    initX = 129
    initY = 56
  end

  local thisBit = {
    x = initX,
    y = initY,
    color = randomColor,
    dir = 'd',
    destroyed = false,
    paused = false,
    kill = function(self)
      if btnp(5) and 's' == self.dir and activeColor == self.color and activeBlaster == trackNum then
        self.destroyed = true
        score += 5
      end
    end,
    move = function(self)
      if self.destroyed then
        return
      end
      -- check if at a chang dir pt
      for chpt in all(track) do
        if self.x == chpt.x and self.y == chpt.y then
          self.dir = chpt.dir
        end
      end

      -- check if need to pause
      self.paused = false
      if prevBit then
        dist = sqrt((prevBit.x - self.x)^2 + (prevBit.y - self.y)^2)
        if prevBit.destroyed == false and dist < 5 and ((self.x == prevBit.x) or (self.y == prevBit.y)) then
          self.paused = true
        end
      end

      -- move bit
      if not self.paused then
        if self.dir == 'd' then
          self.y += 1
        elseif self.dir =='u' then
          self.y -= 1
        elseif self.dir == 'r' then
          self.x += 1
        elseif self.dir == 'l' then
          self.x -= 1
        end
      end
    end,
    render = function(self)
      if self.destroyed then
        return
      end
      -- local bit_sprite
      if self.color == 'r' then
        bit_sprite = 2
      elseif self.color == 'g' then
        bit_sprite = 4
      elseif self.color == 'b' then
        bit_sprite = 1
      else
        bit_sprite = 3
      end
      spr(bit_sprite, self.x, self.y)
    end
  }

  add(bits[trackNum], thisBit)
end

function _init()
  -- add(bits1, {x = 56, y = -5, color = 'r', dir = 'd'})
  score = 0
end

function _update()
  t += 1

  if t % 30 == 0 then
    addBit(flr(rnd(4)) + 1)
    -- add(bits1, {x = 56, y = -5, color = 'r', dir = 'd'})
  end

  -- score
  if t % 32 == 0 then
    score += 1
  end
  -- set active color
  local ogColor = activeColor
  if btn(4) then
    if btnp(0) then
      activeColor = 'b'
    elseif btnp(1) then
      activeColor = 'g'
    elseif btnp(2) then
      activeColor = 'y'
    elseif btnp(3) then
      activeColor = 'r'
    end
  end

  -- set active blaster
  if not btn(4) then
    if btn(0) then --left
      activeBlaster = 2
    end
    if btn(1) then --right
      activeBlaster = 4
    end
    if btn(2) then --up
      activeBlaster = 1
    end
    if btn(3) then --down
      activeBlaster = 3
    end
  end

  -- compute bits
  for bitTable in all(bits) do
    for bit in all(bitTable) do
      bit.move(bit)
      bit.kill(bit)
    end
  end

  if debug then
    if btn(0) then --left
      debugX -= 1
    elseif btn(1) then --right
      debugX += 1
    elseif btn(2) then --up
      debugY -= 1
    elseif btn(3) then --down
      debugY += 1
    end
  end
end

function _draw()
 cls()
 -- rectfill(0,0,128,128,6) -- bg
 map(0, 48, 0, 0, 16, 16) -- lvl 1
 spr(17, 78, 74, 2, 2) -- color game pad
 print("z +", 66, 79, 7)

 -- bits
 for bitTable in all(bits) do
   for bit in all(bitTable) do
     bit.render(bit)
   end
 end

 -- set color
 if activeColor == 'y' then
   pal(7,10)
 elseif activeColor == 'r' then
   pal(7,8)
 elseif activeColor == 'b' then
   pal(7,12)
 elseif activeColor == 'g' then
   pal(7,11)
 end

 spr(19, 56, 48) -- up track
 if activeBlaster == 1 then
   spr(22, 56, 48) -- up halo
 end
 spr(19, 56, 64, 1, 1, false, true) -- down track
 if activeBlaster == 3 then
   spr(22, 56, 64) -- down halo
 end
 spr(20, 64, 56) -- right track
 if activeBlaster == 4 then
   spr(22, 64, 56) -- right halo
 end
 spr(20, 48, 56, 1, 1, true, false) -- left track
 if activeBlaster == 2 then
   spr(22, 48, 56) -- left halo
 end
 spr(21, 56, 56)
 pal()

 print(score, 68, 5, 7)

 if debug then
   pset(0,0,7)
   pset(127,0,7)
   pset(127,127,7)
   pset(0,127,7)
   pset(debugX, debugY, 8)
   print("X: " .. debugX .. "\nY: " .. debugY, debugX + 3, debugY + 3, 12)
 end
end

