#!/bin/bash
if [[ $1 == 'build' ]]; then
    # create pico file from export file is one does not exist
    if [ -e 'game1.p8' ]; then
        echo "Found .p8 file"
        ed -s game1.p8 <<< $'H\n/__lua/+1,/__gfx/-1d\n/__lua/r code.lua\nw'
        echo "built .p8 file"
    elif [ -e 'export.txt' ]; then
        echo "Found export.txt to start build with"
        cp export.txt game1.p8
        ed -s game1.p8 <<< $'/__lua/r code.lua\nw'
        echo "built .p8 file"
    else
        echo "no .p8 file found"
    fi
fi


if [[ $1 == 'export' ]]; then
    # create pico file from export file is one does not exist
    if [ -e 'game1.p8' ]; then
        echo "Found .p8 file"
        echo '/__lua/+1,/__gfx/-1d\n,p\nq' | ed -s game1.p8 > export.txt
        echo "exported export.txt"
    else
        echo "no .p8 file found"
    fi
fi
# elif [ -n $1 ]; then
#     echo "String is not empty"
# fi
# delete the code out of game1.p8 and insert code from code.lua
# ed -s game1.p8 <<< $'H\n/__lua/+1,/__gfx/-1d\n/__lua/r code.lua\nw'

